В общем.
С начала...
поднимаем виртуальное окружение
```cd /path/to/yourbot```
```python3 -m venv env```

Устанавливаем зависимости.
``pip3 install -r requirements.txt``

Вторым шагом мы создаём .env файл в корне проекта (где лежит main.py)
В этот .env файл мы вписываем следующее:
``BOT_TOKEN=<My-Token>``
Без <>


Затем стартуем бота
``python3 main.py``

Теперь пример демона:
```
[Unit]
Description=Bot for EAT
After=network.target

[Service]
User=<your_user>
Group=<your_group>
WorkingDirectory=/path/to/folder
ExecStart=/path/to/env/bin/python3 /path/to/bot_folder/main.py
become: true
[Install]
WantedBy=multi-user.target
```

Готовый бот -> https://t.me/pnfp_bot