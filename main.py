import random
import aiogram
from aiogram import Bot, types
from aiogram import Dispatcher
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, InlineKeyboardMarkup, InlineKeyboardButton
import os
from dotenv import load_dotenv
load_dotenv()

# Токен вашего бота
API_TOKEN = os.getenv('BOT_TOKEN')

# Путь к файлу с рецептами
RECIPES_FILE = 'recipes.txt'

# Инициализация бота и диспетчера
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

# Кнопка "Eat"
eat_button = KeyboardButton("Eat")
buy_coffee_button = KeyboardButton("BuyMeACoffee")
menu = ReplyKeyboardMarkup(resize_keyboard=True)
menu.add(eat_button)
menu.add(buy_coffee_button)

# Кнопка для BuyMeACoffee
def buy_me_a_coffee_button():
    return InlineKeyboardMarkup().add(InlineKeyboardButton("BuyMeACoffee", url="https://www.buymeacoffee.com/orik"))

# Обработчик команды /start
@dp.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.reply("Привет! Я бот для Правильного Питания Программистов. "
                        "Не забудьте учитывать свои аллергии!\n"
                        "Нажмите кнопку 'Eat', чтобы получить одноразовый прием полезной пищи.", reply_markup=menu)

# Обработчик нажатия кнопки "BuyMeACoffee"
@dp.message_handler(lambda message: message.text == "BuyMeACoffee")
async def process_coffee_command(message: types.Message):
    await message.answer("Благодарю за поддержку! Вы можете купить мне кофе по ссылке:", reply_markup=buy_me_a_coffee_button())

# Обработчик нажатия кнопки "Eat"
@dp.message_handler(lambda message: message.text == "Eat")
async def process_eat_command(message: types.Message):
    recipe = get_random_recipe()
    if recipe:
        await message.answer(f"Прием пищи: {recipe}")
 #   else:
 #       await message.answer("Извините, рецепты закончились. Добавьте еще в файл recipes.txt!")


# Функция для получения случайного рецепта из файла
def get_random_recipe():
    try:
        with open(RECIPES_FILE, 'r', encoding='utf-8') as file:
            recipes = file.readlines()
            if recipes:
                recipe = random.choice(recipes).strip()
                return recipe
            else:
                return None
    except FileNotFoundError:
        return None



# Запуск бота
if __name__ == '__main__':
    aiogram.executor.start_polling(dp, skip_updates=True)
